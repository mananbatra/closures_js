function limitFunctionCallCount(callBack, limitNum) {
    let count = 0;

    if(callBack===undefined || limitNum === undefined)
    {
        throw console.error("value undefined");
    }
    if(callBack===null || limitNum === null)
    {
        throw console.error("value undefined");
    }

    return function (...args) {
        if (count < limitNum) {
            count++;
            return callBack(...args);
        } else {
            return null;// Returning null if the limit is reached
        }
    };
}

module.exports=limitFunctionCallCount;