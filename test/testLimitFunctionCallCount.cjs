let limitFunctionCallCount = require("../limitFunctionCallCount.cjs");

function callBack(count) {
    console.log("function called " + count + " times");
    return 0
}

let limit = limitFunctionCallCount(callBack, 4);
try {
    //let b = limit(2);
    //console.log(b); //testing
    limit(4);
    limit(6);
    limit();
    limit();
    limit();
    limit();
}
catch (error) {
    console.log(error);
}