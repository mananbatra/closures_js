let cacheFunction=require('../cacheFunction.cjs');
try{
function callBack(...numbers) {
    console.log("Executing add function ");
    return numbers.reduce((initial, next) => initial + next, 0); // 
  }
  
  const cachedAdd= cacheFunction(callBack);

  console.log(cachedAdd(10));
  console.log(cachedAdd(10));
  console.log(cachedAdd(5,4,3));
  console.log(cachedAdd(5,4,3));
}
catch(error)
{
  console.log(error);
}