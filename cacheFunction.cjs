function cacheFunction(callBack) {
    

    if (typeof callBack !== 'function') {
        //console.log('.................')
        throw new Error("value undefined");
    } 

        const cache = {};
        return function (...args) {
            const key = JSON.stringify(args);

            if (key in cache) {
                console.log("Cache caught- Value already present");
                return cache[key];
            } else {
                
                console.log("Cache left - new value");
                const result = callBack(...args);
                cache[key] = result;
                return result;
            }
        };
    


}

module.exports = cacheFunction;