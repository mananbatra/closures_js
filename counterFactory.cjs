function counterFactory() 
{
    let counter = 0; // Counter variable in closure scope

    return {
        increment: function () {
            counter++;
            return counter;
        },
        decrement: function () {
            counter--;
            return counter;
        }
    };
}

module.exports=counterFactory;